var keyframeFadeOut = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0'
  }
};

var keyframeFadeOutDown = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, 100%, 0)'
  }
};

var keyframeFadeOutDownBig = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, 2000px, 0)'
  }
};

var keyframeFadeOutLeft = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(-100%, 0, 0)'
  }
};

var keyframeFadeOutLeftBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(-2000px, 0, 0)'
  }
};

var keyframeFadeOutRight = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0)'
  }
};

var keyframeFadeOutRightBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(2000px, 0, 0)'
  }
};

var keyframeFadeOutUp = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  }
};

var keyframeFadeOutUpBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, -2000px, 0)'
  }
};

exports.keyframeFadeOut = keyframeFadeOut;
exports.keyframeFadeOutDown = keyframeFadeOutDown;
exports.keyframeFadeOutDownBig = keyframeFadeOutDownBig;
exports.keyframeFadeOutLeft = keyframeFadeOutLeft;
exports.keyframeFadeOutLeftBig = keyframeFadeOutLeftBig;
exports.keyframeFadeOutRight = keyframeFadeOutRight;
exports.keyframeFadeOutRightBig = keyframeFadeOutRightBig;
exports.keyframeFadeOutUp = keyframeFadeOutUp;
exports.keyframeFadeOutUpBig = keyframeFadeOutUpBig;
