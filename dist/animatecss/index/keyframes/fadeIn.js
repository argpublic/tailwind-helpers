var keyframeFadeIn = {
  'from': {
    opacity: '0'
  },
  'to': {
    opacity: '1'
  }
};

var keyframeFadeInDown = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInDownBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -2000px, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInLeft = {
  'from': {
    opacity: '0',
    transform: 'translate3d(-100%, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInLeftBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(-2000px, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInRight = {
  'from': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInRightBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(2000px, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInUp = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInUpBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, 2000px, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

exports.keyframeFadeIn = keyframeFadeIn;
exports.keyframeFadeInDown = keyframeFadeInDown;
exports.keyframeFadeInDownBig = keyframeFadeInDownBig;
exports.keyframeFadeInLeft = keyframeFadeInLeft;
exports.keyframeFadeInLeftBig = keyframeFadeInLeftBig;
exports.keyframeFadeInRight = keyframeFadeInRight;
exports.keyframeFadeInRightBig = keyframeFadeInRightBig;
exports.keyframeFadeInUp = keyframeFadeInUp;
exports.keyframeFadeInUpBig = keyframeFadeInUpBig;
