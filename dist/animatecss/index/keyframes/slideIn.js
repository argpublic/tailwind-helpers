var keyframeSlideInDown = {
  'from': {
    transform: 'translate3d(0, -100%, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInLeft = {
  'from': {
    transform: 'translate3d(-100%, 0, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInRight = {
  'from': {
    transform: 'translate3d(100%, 0, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInUp = {
  'from': {
    transform: 'translate3d(0, 100%, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

exports.keyframeSlideInDown = keyframeSlideInDown;
exports.keyframeSlideInLeft = keyframeSlideInLeft;
exports.keyframeSlideInRight = keyframeSlideInRight;
exports.keyframeSlideInUp = keyframeSlideInUp;
