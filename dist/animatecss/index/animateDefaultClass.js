function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

module.exports = function (e) {
  var _ref;

  return _ref = {
    '.animated': {
      'animation-duration': '1s',
      'animation-fill-mode': 'both'
    },

    '.animated.infinite': {
      'animation-iteration-count': 'infinite'
    }

  }, _defineProperty(_ref, '.animated.' + e('delay-1/4s'), {
    'animation-delay': '.25s'
  }), _defineProperty(_ref, '.animated.' + e('delay-2/4s'), {
    'animation-delay': '.5s'
  }), _defineProperty(_ref, '.animated.' + e('delay-3/4s'), {
    'animation-delay': '.75s'
  }), _defineProperty(_ref, '.animated.delay-1s', {
    'animation-delay': '1s'
  }), _defineProperty(_ref, '.animated.delay-2s', {
    'animation-delay': '2s'
  }), _defineProperty(_ref, '.animated.delay-3s', {
    'animation-delay': '3s'
  }), _defineProperty(_ref, '.animated.delay-4s', {
    'animation-delay': '4s'
  }), _defineProperty(_ref, '.animated.delay-5s', {
    'animation-delay': '5s'
  }), _defineProperty(_ref, '.animated.fast', {
    'animation-duration': '800ms'
  }), _defineProperty(_ref, '.animated.faster', {
    'animation-duration': '500ms'
  }), _defineProperty(_ref, '.animated.slow', {
    'animation-duration': '2s'
  }), _defineProperty(_ref, '.animated.slower', {
    'animation-duration': '3s'
  }), _defineProperty(_ref, '@media (print), (prefers-reduced-motion: reduce)', {
    '.animated': {
      'animation-duration': '1ms !important',
      'transition-duration': '1ms !important',
      'animation-iteration-count': '1 !important'
    }
  }), _ref;
};
