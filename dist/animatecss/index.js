function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var defaultClass = require('./index/animateDefaultClass');
var classes = require('./index/animateClasses');
var assign = require('lodash/assign');
var forEach = require('lodash/forEach');

module.exports = function () {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  return function (_ref) {
    var addUtilities = _ref.addUtilities,
        variants = _ref.variants,
        e = _ref.e;

    var result = {};

    forEach(options, function (animation) {
      var _assign;

      classes['.' + animation] && assign(result, (_assign = {}, _defineProperty(_assign, '.' + animation, classes['.' + animation]), _defineProperty(_assign, '@keyframes ' + animation, classes['@keyframes ' + animation]), _assign));
    });

    if (Object.keys(result).length) {
      assign(result, defaultClass(e));
      addUtilities(result);
    }
  };
};
