var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable */

function html() {
    return {
        'html': {
            'min-height': '100%',
            'width': '100%'
        }
    };
}

function body() {
    return {
        'body': {
            'min-height': '100%'
        }
    };
}

function removeNativeRemoveButtonFromSearchInput() {
    return _defineProperty({
        /* clears the 'X' from Internet Explorer */
        'input[type=search]::-ms-clear': {
            display: 'none',
            width: 0,
            height: 0
        },
        'input[type=search]::-ms-reveal': {
            display: 'none',
            width: 0,
            height: 0
        }
    }, 'input[type="search"]::-webkit-search-decoration,' + 'input[type="search"]::-webkit-search-cancel-button,' + 'input[type="search"]::-webkit-search-results-button,' + 'input[type="search"]::-webkit-search-results-decoration', {
        display: 'none'
    });
}

function headings() {
    var _ref2;

    return _ref2 = {}, _defineProperty(_ref2, 'h1, h2, h3, h4, h5, h6,' + '.h1, .h2, .h3, .h4, .h5, .h6', {
        'font-family': 'inherit',
        'font-weight': '500',
        'line-height': '1.1',
        color: 'inherit'
    }), _defineProperty(_ref2, 'h1 small,' + 'h1 .small, h2 small,' + 'h2 .small, h3 small,' + 'h3 .small, h4 small,' + 'h4 .small, h5 small,' + 'h5 .small, h6 small,' + 'h6 .small,' + '.h1 small,' + '.h1 .small, .h2 small,' + '.h2 .small, .h3 small,' + '.h3 .small, .h4 small,' + '.h4 .small, .h5 small,' + '.h5 .small, .h6 small,' + '.h6 .small', {
        'font-weight': '400',
        'line-height': '1',
        color: '#777777'
    }), _defineProperty(_ref2, 'h1, .h1,' + 'h2, .h2,' + 'h3, .h3', {
        'margin-top': '20px',
        'margin-bottom': '10px'
    }), _defineProperty(_ref2, 'h1 small,' + 'h1 .small, .h1 small,' + '.h1 .small,' + 'h2 small,' + 'h2 .small, .h2 small,' + '.h2 .small,' + 'h3 small,' + 'h3 .small, .h3 small,' + '.h3 .small', {
        'font-size': '65%'
    }), _defineProperty(_ref2, 'h4, .h4,' + 'h5, .h5,' + 'h6, .h6', {
        'margin-top': '10px',
        'margin-bottom': '10px'
    }), _defineProperty(_ref2, 'h4 small,' + 'h4 .small, .h4 small,' + '.h4 .small,' + 'h5 small,' + 'h5 .small, .h5 small,' + '.h5 .small,' + 'h6 small,' + 'h6 .small, .h6 small,' + '.h6 .small', {
        'font-size': '75%'
    }), _defineProperty(_ref2, 'h1, .h1', {
        'font-size': '36px'
    }), _defineProperty(_ref2, 'h2, .h2', {
        'font-size': '30px'
    }), _defineProperty(_ref2, 'h3, .h3', {
        'font-size': '24px'
    }), _defineProperty(_ref2, 'h4, .h4', {
        'font-size': '18px'
    }), _defineProperty(_ref2, 'h5, .h5', {
        'font-size': '14px'
    }), _defineProperty(_ref2, 'h6, .h6', {
        'font-size': '12px'
    }), _defineProperty(_ref2, '.text-lowercase', {
        'text-transform': 'lowercase'
    }), _defineProperty(_ref2, '.text-capitalize', {
        'text-transform': 'capitalize'
    }), _defineProperty(_ref2, '.text-muted', {
        color: '#777777'
    }), _ref2;
}

module.exports = function (_ref3) {
    var addBase = _ref3.addBase,
        config = _ref3.config;

    addBase(_extends({}, html(), body(), headings(), removeNativeRemoveButtonFromSearchInput()));
};
