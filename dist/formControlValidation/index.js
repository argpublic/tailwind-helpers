function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable */

var assign = require('lodash/assign');

var _require = require('./../helpers/lightenDarkenColor'),
    darken = _require.darken,
    lighten = _require.lighten;

var forEach = require('lodash/forEach');
var merge = require('lodash/merge');
var map = require('lodash/map');

function formControlValidation(e, screens, color) {
	var _ref;

	return _ref = {
		position: 'relative'
	}, _defineProperty(_ref, '.form-input,' + '.form-textarea,' + '.form-select,' + '.form-multiselect,' + '.form-checkbox,' + '.form-radio,' + '.form-control', {
		'border-color': color,
		'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, .075)',
		'&:focus': {
			'border-color': darken(color, 10),
			'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px ' + lighten(color, 20)
		}
	}), _defineProperty(_ref, '.hint, .help-block', {
		color: color
	}), _ref;
}

function hint() {
	return {
		'.hint': {
			display: 'block',
			'margin-top': '5px',
			'margin-bottom': '10px',
			'border-radius': '5px',
			'&:before': {
				width: 0,
				height: 0,
				'border-style': 'solid',
				'border-width': '10px 12px 0',
				top: '100%',
				right: '35px',
				position: 'absolute'
			}
		}
	};
}

function hintScreen(e, screen, colors) {

	screen = screen ? '.' + e(screen + ':hint-show') : '.hint-show';

	var hintShow = _defineProperty({}, screen, {
		position: 'absolute',
		right: '100%',
		bottom: '100%',
		margin: 0,
		'margin-right': '-150px',
		'margin-bottom': '10px',
		'white-space': 'nowrap',
		padding: '9px 25px',
		'z-index': 2,
		'&:before': {
			content: '""'
		}
	});

	var hintColors = {};

	forEach(colors, function (color) {
		assign(hintColors, hintColor(e, screen, color.color, color.name));
	});

	return merge(hintShow, hintColors);
}

function hintColor(e, screen, color, name) {

	return _defineProperty({}, screen + '.hint-' + name, {
		background: color,
		color: '#fff',
		'&:before': {
			'border-color': color + ' transparent transparent'
		}
	});
}

module.exports = function () {
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

	return function (_ref3) {
		var addComponents = _ref3.addComponents,
		    e = _ref3.e,
		    theme = _ref3.theme;

		var result = {};

		var screens = theme('screens', {});

		var _options$colors = options.colors,
		    colors = _options$colors === undefined ? [] : _options$colors;


		forEach(colors, function (color) {
			assign(result, _defineProperty({}, '.has-' + color.name, formControlValidation(e, screens, color.color)));
		});

		merge(result, hint(), hintScreen(e, '', colors));

		var hintMediaQueries = {};

		forEach(screens, function (width, name) {
			assign(hintMediaQueries, _defineProperty({}, '@media (min-width: ' + width + ')', hintScreen(e, name, colors)));
		});

		assign(result, hintMediaQueries);

		addComponents(result, { respectPrefix: false });
	};
};
