var _require = require('../../helpers/lightenDarkenColor'),
    darken = _require.darken;

module.exports = function () {
  return {
    '.btn-spoiler': {
      color: '#337ab7',
      'font-weight': 'normal',
      'border-radius': 0,
      padding: 0
    },

    '.btn-spoiler,.btn-spoiler:active, .btn-spoiler.active, .btn-spoiler[disabled], fieldset[disabled] .btn-spoiler': {
      'background-color': 'transparent',
      'webkit-box-shadow': 'none',
      'box-shadow': 'none'
    },

    '.btn-spoiler, .btn-spoiler:hover, .btn-spoiler:focus, .btn-spoiler:active': {
      'border-color': 'transparent'
    },

    '.btn-spoiler:hover, .btn-spoiler:focus': {
      color: darken('#337ab7', 10),
      'text-decoration': 'none',
      'background-color': 'transparent'
    },

    '.btn-spoiler[disabled]:hover, .btn-spoiler[disabled]:focus, fieldset[disabled] .btn-spoiler:hover, fieldset[disabled] .btn-spoiler:focus': {
      color: '#777',
      'text-decoration': 'none'
    },

    '.btn-spoiler:after': {
      left: '0',
      right: '0',
      top: '50%',
      'margin-top': '.8em',
      content: '""',
      position: 'absolute',
      'border-bottom': '1px dashed #1ca2bd',
      'z-index': '1'
    }

  };
};
