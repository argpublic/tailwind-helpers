var _require = require('../../helpers/lightenDarkenColor'),
    lighten = _require.lighten,
    darken = _require.darken;

var $lightGrey = lighten('#c8c7cc', 18);

module.exports = function (_ref) {
  var $color = _ref.color,
      _ref$fontColor = _ref.fontColor,
      $fontColor = _ref$fontColor === undefined ? '#ffffff' : _ref$fontColor;

  return {
    '.open &': {
      '&.dropdown-toggle': {
        'background-color': lighten($color, 15),
        'border-color': lighten($color, 15)
      }
    },
    'background-color': $color,
    'border-color': $color,
    color: $fontColor,

    '&:hover': {
      'background-color': lighten($color, 10) + ' !important',
      'border-color': lighten($color, 10),
      color: $fontColor
    },

    '&:active, &.active, &.active:focus, &:active:focus, &:active:hover, &.dropdown-toggle:active:hover': {
      'background-color': darken($color, 5) + ' !important',
      'border-color': darken($color, 5)
    },

    '&:focus': {
      'background-color': lighten($color, 15),
      'border-color': lighten($color, 15)
    },

    '&.disabled:hover, &.disabled:focus, &.disabled:active, &.disabled.active, &[disabled], &[disabled]:hover, &[disabled]:focus, &[disabled]:active, &[disabled].active, fieldset[disabled] &:hover, fieldset[disabled] &:focus, fieldset[disabled] &:active, fieldset[disabled] &.active': {
      'background-color': lighten($color, 20),
      'border-color': lighten($color, 20),
      color: $fontColor
    },

    '&.btn-o': {
      border: '1px solid ' + $color,
      color: $color,

      '&:hover': {
        color: lighten($color, 10),
        'border-color': lighten($color, 10),
        background: 'none !important'
      },

      '&:active, &.active, &.active:focus, &:active:focus, &:active:hover': {
        'background-color': darken($lightGrey, 5) + ' !important',
        'border-color': darken($color, 15) + ' !important',
        color: darken($color, 15) + ' !important'
      },

      '&:focus': {
        'background-color': lighten($color, 20),
        'border-color': lighten($color, 20),
        color: lighten($color, 10)
      },

      '&.disabled:hover, &.disabled:focus, &.disabled:active, &.disabled.active, &[disabled], &[disabled]:hover, &[disabled]:focus, &[disabled]:active, &[disabled].active, fieldset[disabled] &:hover, fieldset[disabled] &:focus, fieldset[disabled] &:active, fieldset[disabled] &.active': {
        'border-color': lighten($color, 20),
        color: lighten($color, 20)
      },

      '.caret': {
        'border-top-color': $color
      }
    },

    '.caret': {
      'border-top-color': $fontColor
    },

    '.dropup &': {
      '.caret': {
        'border-bottom': '4px solid ' + $fontColor
      }
    }
  };
};
