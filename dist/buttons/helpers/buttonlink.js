var _require = require('../../helpers/lightenDarkenColor'),
    darken = _require.darken;

module.exports = function () {
  return {
    '.btn-link': {
      color: '#337ab7',
      'font-weight': 'normal',
      'border-radius': 0
    },

    '.btn-link,.btn-link:active, .btn-link.active, .btn-link[disabled], fieldset[disabled] .btn-link': {
      'background-color': 'transparent',
      'webkit-box-shadow': 'none',
      'box-shadow': 'none'
    },

    '.btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active': {
      'border-color': 'transparent'
    },

    '.btn-link:hover, .btn-link:focus': {
      color: darken('#337ab7', 10),
      'text-decoration': 'underline',
      'background-color': 'transparent'
    },

    '.btn-link[disabled]:hover, .btn-link[disabled]:focus, fieldset[disabled] .btn-link:hover, fieldset[disabled] .btn-link:focus': {
      color: '#777',
      'text-decoration': 'none'
    }

  };
};
