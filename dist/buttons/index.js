function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable global-require,import/no-extraneous-dependencies */

var assign = require('lodash/assign');
var merge = require('lodash/merge');
var forEach = require('lodash/forEach');

var create = require('./helpers/buttonCreator');
var btnLink = require('./helpers/buttonlink');
var btnSpoiler = require('./helpers/buttonSpoiler');
var opacity = require('./../helpers/opacity');

function btnBootstrap(_ref) {
  var _ref$btnFontWeight = _ref.btnFontWeight,
      btnFontWeight = _ref$btnFontWeight === undefined ? 'normal' : _ref$btnFontWeight,
      _ref$btnDefaultColor = _ref.btnDefaultColor,
      _ref$cursorDisabled = _ref.cursorDisabled,
      cursorDisabled = _ref$cursorDisabled === undefined ? 'not-allowed' : _ref$cursorDisabled;

  return {
    '.btn': {
      display: 'inline-block',
      'font-weight': btnFontWeight,
      'text-align': 'center',
      'vertical-align': 'middle',
      'touch-action': 'manipulation',
      cursor: 'pointer',
      'background-image': 'none', // Reset unusual Firefox-on-Android default style; see https://github.com/necolas/normalize.css/issues/214
      border: '1px solid transparent',
      'white-space': 'nowrap',
      padding: '8px 12px',
      'font-size': '14px',
      'line-height': '1.42857143',
      'border-radius': '2px',
      'user-select': 'none',

      '&:active,&.active': {
        '&:focus,&.focus': require('./../helpers/tabFocus')
      },

      '&:hover,&:focus, &.focus': {
        'text-decoration': 'none'
      },

      '&:active, &.active': {
        outline: 0,
        'background-image': 'none',
        'box-shadow': 'inset 0 3px 5px rgba(0, 0, 0, 0.125)'
      },

      '&.disabled,&[disabled],fieldset[disabled] &': assign({
        cursor: cursorDisabled,
        'box-shadow': 'none'
      }, opacity(0.65))

      // [converter] extracted a& to a.btn
    }
  };
}

function btnCustom() {
  return {
    '.btn': {
      transition: 'all 0.3s ease 0s !important',
      'background-image': 'none !important',
      'box-shadow': 'none !important',
      outline: 'none !important',
      position: 'relative',

      '&:hover, &:focus, &:active, &.active, &.disabled, &[disabled]': {
        'box-shadow': 'none'
      },

      '&:after': {
        content: '""',
        position: 'absolute',
        transition: 'all 0.3s ease 0s',
        'z-index': '-1'
      }
    }
  };
}

function btnSize(name, options) {
  return _defineProperty({}, '.btn-' + name, options);
}

function btnWide() {
  return {
    '.btn-wide': {
      'min-width': '120px'
    }
  };
}

function btnO() {
  return {
    '.btn-o': {
      background: 'none !important'
    }
  };
}

function btnSquared() {
  return {
    '.btn-squared': {
      'border-radius': '0 !important'
    }
  };
}

function btnGroup() {
  return {
    '.btn-group .btn + .btn, .btn-group .btn + .btn-group, .btn-group .btn-group + .btn, .btn-group .btn-group + .btn-group': {
      'border-left-color': 'rgba(255, 255, 255, 0.5) !important',
      margin: 0
    }
  };
}

function btnGroupVertical() {
  return {
    '.btn-group-vertical > .btn + .btn, .btn-group-vertical > .btn + .btn-group, .btn-group-vertical > .btn-group + .btn, .btn-group-vertical > .btn-group + .btn-group': {
      'border-top-color': 'rgba(255, 255, 255, 0.5) !important',
      margin: 0
    }
  };
}

module.exports = function () {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return function (_ref3) {
    var addComponents = _ref3.addComponents;

    var result = {};

    assign(result, btnBootstrap(options));

    merge(result, btnCustom());

    var sizes = options.sizes || {};

    assign(result, btnSize('lg', sizes.lg || {
      padding: '0.5rem 1rem',
      fontSize: '1.25rem',
      lineHeight: 1.5,
      borderRadius: '0.3rem'
    }));

    assign(result, btnSize('sm', sizes.sm || {
      padding: '0.25rem 0.5rem',
      fontSize: '0.875rem',
      lineHeight: 1.5,
      borderRadius: '0.2rem'
    }));

    assign(result, btnWide());

    assign(result, btnO());

    assign(result, btnSquared());

    assign(result, btnGroup());

    assign(result, btnGroupVertical());

    var _options$buttons = options.buttons,
        buttons = _options$buttons === undefined ? [] : _options$buttons;


    forEach(buttons, function (button) {
      assign(result, _defineProperty({}, '.btn-' + button.name, create(button)));
    });

    assign(result, btnLink());

    assign(result, btnSpoiler());

    addComponents(result, { respectPrefix: false });
  };
};
