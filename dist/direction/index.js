module.exports = function (_ref) {
  var addUtilities = _ref.addUtilities;

  addUtilities({
    '.rtl': {
      direction: 'rtl'
    },
    '.ltr': {
      direction: 'ltr'
    }
  }, { respectPrefix: false });
};
