module.exports = function ($opacity) {
  // Opacity

  // IE8 filter
  var $opacityIe = $opacity * 100;

  return {
    opacity: $opacity,
    filter: "alpha(opacity=" + $opacityIe + ")"
  };
};
