var _ = require('lodash');
var defaultOptions = require('./defaultOptions');

module.exports = function () {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$options = _ref.options,
      options = _ref$options === undefined ? [] : _ref$options;

  return function (_ref2) {
    var addUtilities = _ref2.addUtilities;

    var selectedOptions = !_.isEmpty(options) ? options : _.keys(defaultOptions);

    var utilities = _.chain(selectedOptions).map(function (option) {
      return defaultOptions[option];
    }).flattenDeep();

    addUtilities(utilities.value());
  };
};
