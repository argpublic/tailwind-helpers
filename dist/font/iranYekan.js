module.exports = function (_ref) {
  var addUtilities = _ref.addUtilities;

  var newUtilities = {
    '.font-yekan': {
      'font-family': 'IRANYekan,sans-serif'
    }
  };

  addUtilities(newUtilities);
};
