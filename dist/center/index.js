module.exports = function (_ref) {
  var addUtilities = _ref.addUtilities,
      variants = _ref.variants;

  var newUtilities = {
    '.center-left': {
      left: '50%',
      transform: 'translateX(-50%)'
    },
    '.center-top': {
      top: '50%',
      transform: 'translateY(-50%)'
    },
    '.center': {
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  };

  addUtilities(newUtilities, variants('centerPlugin'));
};
