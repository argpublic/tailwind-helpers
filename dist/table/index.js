var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable */
var forEach = require('lodash/forEach');

var _require = require('./../helpers/lightenDarkenColor'),
    darken = _require.darken;

function optionsWithPropertyExists(options, property) {
	return typeof options !== 'undefined' && options.hasOwnProperty(property);
}

function initClasses(options) {
	var _ref;

	return _ref = {
		'table': {
			borderCollapse: 'collapse'
		},

		'table col[class*="col-"]': {
			position: 'static',
			display: 'table-column',
			float: 'none'
		}

	}, _defineProperty(_ref, 'table td[class*="col-"]' + ',table th[class*="col-"]', {
		position: 'static',
		display: 'table-cell',
		float: 'none'
	}), _defineProperty(_ref, 'caption', {
		paddingTop: '.75rem',
		paddingBottom: '.75rem',
		color: '#6c757d',
		textAlign: 'left',
		captionSide: 'bottom'
	}), _defineProperty(_ref, 'th', {
		textAlign: 'inherit'
	}), _defineProperty(_ref, '.table', {
		width: '100%',
		maxWidth: '100%',
		marginBottom: '1rem',
		backgroundColor: 'transparent',
		'td,th': {
			padding: optionsWithPropertyExists(options, 'cellPadding') ? options.cellPadding : '.75rem',
			verticalAlign: optionsWithPropertyExists(options, 'verticalAlign') ? options.verticalAlign : 'top',
			borderTop: optionsWithPropertyExists(options, 'tableBorderColor') ? '1px solid ' + options.tableBorderColor : '1px solid #dee2e6'
		},
		'tbody': {
			verticalAlign: 'inherit'
		},
		'td': {
			borderTop: optionsWithPropertyExists(options, 'tableBodyBorder') && options.tableBodyBorder == false ? 0 : optionsWithPropertyExists(options, 'tableBorderColor') ? '1px solid ' + options.tableBorderColor : '1px solid #dee2e6'
		},
		'thead th': {
			verticalAlign: 'bottom',
			borderBottom: optionsWithPropertyExists(options, 'tableBorderColor') ? '2px solid ' + options.tableBorderColor : '2px solid #dee2e6'
		},
		'tbody+tbody': {
			borderTop: optionsWithPropertyExists(options, 'tableBorderColor') ? '2px solid ' + options.tableBorderColor : '2px solid #dee2e6'
		},
		'.table': {
			backgroundColor: '#fff'
		}
	}), _defineProperty(_ref, '.table-sm td,.table-sm th', {
		padding: '.3rem'
	}), _defineProperty(_ref, '.table-bordered', {
		border: optionsWithPropertyExists(options, 'tableBorderColor') ? '1px solid ' + options.tableBorderColor : '1px solid #dee2e6',
		'td,th': {
			border: optionsWithPropertyExists(options, 'tableBorderColor') ? '1px solid ' + options.tableBorderColor : '1px solid #dee2e6'
		},
		'thead td,thead th': {
			borderBottomWidth: '2px'
		}
	}), _defineProperty(_ref, '.table-borderless', {
		'th, td, thead th, tbody + tbody': {
			border: 0
		}
	}), _defineProperty(_ref, '.table-striped tbody tr:nth-of-type(odd)', {
		backgroundColor: optionsWithPropertyExists(options, 'tableStripedBackgroundColor') ? options.tableStripedBackgroundColor : 'rgba(0,0,0,.05)'
	}), _defineProperty(_ref, '.table-hover tbody tr:hover', {
		backgroundColor: optionsWithPropertyExists(options, 'tableHoverBackgroundColor') ? options.tableHoverBackgroundColor : 'rgba(0,0,0,.075)'
	}), _ref;
}

function states(options) {
	var result = {};
	forEach(options.states, function (state, name) {
		var _extends2;

		result = _extends({}, result, (_extends2 = {}, _defineProperty(_extends2, '.table-' + name + ',.table-' + name + '>td,.table-' + name + '>th', {
			backgroundColor: state.color
		}), _defineProperty(_extends2, '.table-hover .table-' + name + ':hover', {
			backgroundColor: state.hover || darken(state.color, 5)
		}), _defineProperty(_extends2, '.table-hover .table-' + name + ':hover>td,.table-hover .table-' + name + ':hover>th', {
			backgroundColor: state.hover || darken(state.color, 5)
		}), _extends2));
	});
	return result;
}

function darkHelper(options) {
	if (!options.states || !options.states.dark) return {};
	return {
		'.table .thead-dark th': {
			color: '#fff',
			backgroundColor: '#212529',
			borderColor: '#32383e'
		},
		'.table-dark': {
			color: '#fff',
			backgroundColor: '#212529'
		},
		'.table-dark td,.table-dark th,.table-dark thead th': {
			borderColor: '#32383e'
		},
		'.table-dark.table-bordered': {
			border: '0'
		},
		'.table-dark.table-striped tbody tr:nth-of-type(odd)': {
			backgroundColor: 'rgba(255,255,255,.05)'
		},
		'.table-dark.table-hover tbody tr:hover': {
			backgroundColor: 'rgba(255,255,255,.075)'
		}
	};
}

function lightHelper(options) {
	if (!options.states || !options.states.light) return {};
	return {
		'.table .thead-light th': {
			color: '#495057',
			backgroundColor: '#e9ecef',
			borderColor: optionsWithPropertyExists(options, 'tableBorderColor') ? options.tableBorderColor : '#dee2e6'
		}
	};
}

function responsiveAndBorderLessHelpers() {
	return {
		'.table-responsive': {
			display: 'block',
			width: '100%',
			overflowX: 'auto',
			'-webkit-overflow-scrolling': 'touch',
			'-ms-overflow-style': '-ms-autohiding-scrollbar'
		},
		'.table-responsive>.table-bordered': {
			border: '0'
		},
		'.table-borderless tbody+tbody,.table-borderless td,.table-borderless th,.table-borderless thead th': {
			border: '0'
		}
	};
}

function mediaQueries(screens) {

	function medialQuery(name) {
		var _ref2;

		return _ref2 = {}, _defineProperty(_ref2, '.table-responsive' + (name ? '-' + name : ''), {
			display: 'block',
			width: '100%',
			overflowX: 'auto',
			'-webkit-overflow-scrolling': 'touch',
			'-ms-overflow-style': '-ms-autohiding-scrollbar'
		}), _defineProperty(_ref2, '.table-responsive-' + (name ? '-' + name : '') + '>.table-bordered', {
			border: '0'
		}), _ref2;
	}

	var mediaQueries = {};

	forEach(screens, function (width, name) {
		mediaQueries = _extends({}, mediaQueries, _defineProperty({}, '@media (max-width: ' + (width.replace('px', '') - 0.2) + 'px)', medialQuery(name)));
	});

	return _extends({}, mediaQueries, medialQuery());
}

module.exports = function () {
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

	return function (_ref3) {
		var addComponents = _ref3.addComponents,
		    theme = _ref3.theme;


		var screens = theme('screens', {});

		addComponents(_extends({}, initClasses(options), states(options), darkHelper(options), lightHelper(options), responsiveAndBorderLessHelpers(), mediaQueries(screens)));
	};
};
