var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var clearFix = require('./../helpers/clearFix');

module.exports = function (_ref) {
  var addComponents = _ref.addComponents,
      theme = _ref.theme;

  var newUtilities = {
    '.form-group': _extends({
      marginBottom: theme('spacing.4')
    }, clearFix())
  };

  addComponents(newUtilities);
};
