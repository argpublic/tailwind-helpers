import assign from 'lodash/assign';
import 'lodash/forEach';
import 'lodash/merge';
import 'lodash/map';
import 'lodash';

var keyframeBounce = {
  'from, 20%, 53%, 80%, to': {
    animationTimingFunction: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
    transform: 'translate3d(0, 0, 0)'
  },
  '40%, 43%': {
    animationTimingFunction: 'cubic-bezier(0.755, 0.05, 0.855, 0.06)',
    transform: 'translate3d(0, -30px, 0)'
  },
  '70%': {
    animationTimingFunction: 'cubic-bezier(0.755, 0.05, 0.855, 0.06)',
    transform: 'translate3d(0, -15px, 0)'
  },
  '90%': {
    transform: 'translate3d(0, -4px, 0)'
  }
};

var keyframeFlash = {
  'from, 50%, to': {
    opacity: '1'
  },
  '25%, 75%': {
    opacity: '0'
  }
};

var keyframePulse = {
  'from': {
    transform: 'scale3d(1, 1, 1)'
  },
  '50%': {
    transform: 'scale3d(1.05, 1.05, 1.05)'
  },
  'to': {
    transform: 'scale3d(1, 1, 1)'
  }
};

var keyframeRubberBand = {
  'from': {
    transform: 'scale3d(1, 1, 1)'
  },
  '30%': {
    transform: 'scale3d(1.25, 0.75, 1)'
  },
  '40%': {
    transform: 'scale3d(0.75, 1.25, 1)'
  },
  '50%': {
    transform: 'scale3d(1.15, 0.85, 1)'
  },
  '65%': {
    transform: 'scale3d(0.95, 1.05, 1)'
  },
  '75%': {
    transform: 'scale3d(1.05, 0.95, 1)'
  },
  'to': {
    transform: 'scale3d(1, 1, 1)'
  }
};

var keyframeShake = {
  'from, to': {
    transform: 'translate3d(0, 0, 0)'
  },
  '10%, 30%, 50%, 70%, 90%': {
    transform: 'translate3d(-10px, 0, 0)'
  },
  '20%, 40%, 60%, 80%': {
    transform: 'translate3d(10px, 0, 0)'
  }
};

var keyframeHeadShake = {
  '0%': {
    transform: 'translateX(0)'
  },
  '6.5%': {
    transform: 'translateX(-6px) rotateY(-9deg)'
  },
  '18.5%': {
    transform: 'translateX(5px) rotateY(7deg)'
  },
  '31.5%': {
    transform: 'translateX(-3px) rotateY(-5deg)'
  },
  '43.5%': {
    transform: 'translateX(2px) rotateY(3deg)'
  },
  '50%': {
    transform: 'translateX(0)'
  }
};

var keyframeSwing = {
  '20%': {
    transform: 'rotate3d(0, 0, 1, 15deg)'
  },
  '40%': {
    transform: 'rotate3d(0, 0, 1, -10deg)'
  },
  '60%': {
    transform: 'rotate3d(0, 0, 1, 5deg)'
  },
  '80%': {
    transform: 'rotate3d(0, 0, 1, -5deg)'
  },
  'to': {
    transform: 'rotate3d(0, 0, 1, 0deg)'
  }
};

var keyframeTada = {
  'from': {
    transform: 'scale3d(1, 1, 1)'
  },
  '10%, 20%': {
    transform: 'scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg)'
  },
  '30%, 50%, 70%, 90%': {
    transform: 'scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg)'
  },
  '40%, 60%, 80%': {
    transform: 'scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg)'
  },
  'to': {
    transform: 'scale3d(1, 1, 1)'
  }
};

var keyframeWobble = {
  'from': {
    transform: 'translate3d(0, 0, 0)'
  },
  '15%': {
    transform: 'translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg)'
  },
  '30%': {

    transform: 'translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg)'
  },
  '45%': {
    transform: 'translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg)'
  },
  '60%': {
    transform: 'translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg)'
  },
  '75%': {
    transform: 'translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeJello = {
  'from, 11.1% to': {
    transform: 'translate3d(0, 0, 0)'
  },
  '22.2%': {
    transform: 'skewX(-12.5deg) skewY(-12.5deg)'
  },
  '33.3%': {

    transform: 'skewX(6.25deg) skewY(6.25deg)'
  },
  '44.4%': {
    transform: 'skewX(-3.125deg) skewY(-3.125deg)'
  },
  '55.5%': {
    transform: 'skewX(1.5625deg) skewY(1.5625deg)'
  },
  '66.6%': {
    transform: 'skewX(-0.78125deg) skewY(-0.78125deg)'
  },
  '77.7%': {
    transform: 'skewX(0.390625deg) skewY(0.390625deg)'
  },
  '88.8%': {
    transform: 'skewX(-0.1953125deg) skewY(-0.1953125deg)'
  }
};

var keyframeHeartBeat = {
  '0%': {
    transform: 'scale(1)'
  },
  '14%': {
    transform: 'scale(1.3)'
  },
  '28%': {
    transform: 'scale(1)'
  },
  '42%': {
    transform: 'scale(1.3)'
  },
  '70%': {
    transform: 'scale(1)'
  }
};

var keyframeHinge = {
  '0%': {
    transformOrigin: 'top left',
    animationTimingFunction: 'ease-in-out'
  },
  '20%, 60%': {
    transform: 'rotate3d(0, 0, 1, 80deg)',
    transformOrigin: 'top left',
    animationTimingFunction: 'ease-in-out'
  },
  '40%, 80%': {
    transform: 'rotate3d(0, 0, 1, 60deg)',
    transformOrigin: 'top left',
    animationTimingFunction: 'ease-in-out'
  },
  'to': {
    transform: 'translate3d(0, 700px, 0)',
    opacity: '0'
  }
};

var keyframeJackInTheBox = {
  'from': {
    opacity: '0',
    transformOrigin: 'center bottom',
    transform: 'scale(0.1) rotate(30deg)'
  },
  '50%': {
    transform: 'rotate(-10deg)'
  },
  '70%': {
    transform: 'rotate(3deg)'
  },
  'to': {
    transform: 'scale(1)'
  }
};

var keyframeBounce_1 = keyframeBounce;
var keyframeFlash_1 = keyframeFlash;
var keyframePulse_1 = keyframePulse;
var keyframeRubberBand_1 = keyframeRubberBand;
var keyframeShake_1 = keyframeShake;
var keyframeHeadShake_1 = keyframeHeadShake;
var keyframeSwing_1 = keyframeSwing;
var keyframeTada_1 = keyframeTada;
var keyframeWobble_1 = keyframeWobble;
var keyframeJello_1 = keyframeJello;
var keyframeHeartBeat_1 = keyframeHeartBeat;
var keyframeHinge_1 = keyframeHinge;
var keyframeJackInTheBox_1 = keyframeJackInTheBox;

var keyframeLightSpeedIn = {
  'from': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0) skewX(-30deg)'
  },
  '60%': {
    opacity: '1',
    transform: 'skewX(20deg)'
  },
  '80%': {
    transform: 'skewX(-5deg)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeLightSpeedOut = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0) skewX(30deg)'
  }
};

var keyframeLightSpeedIn_1 = keyframeLightSpeedIn;
var keyframeLightSpeedOut_1 = keyframeLightSpeedOut;

var keyframeFlip = {
  'from': {
    transform: 'perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, -360deg)',
    animationTimingFunction: 'ease-out'
  },
  '40%': {
    transform: 'perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg)',
    animationTimingFunction: 'ease-out'
  },
  '50%': {
    transform: 'perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg)',
    animationTimingFunction: 'ease-in'
  },
  '80%': {
    transform: 'perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg)',
    animationTimingFunction: 'ease-in'
  },
  'to': {
    transform: 'perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg)',
    animationTimingFunction: 'ease-in'
  }
};

var keyframeFlipInX = {
  'from': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
    animationTimingFunction: 'ease-in',
    opacity: '0'
  },
  '40%': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, -20deg)',
    animationTimingFunction: 'ease-in'
  },
  '60%': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, 10deg)',
    opacity: '1'
  },
  '80%': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, -5deg)'
  },
  'to': {
    transform: 'perspective(400px)'
  }
};

var keyframeFlipInY = {
  'from': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)',
    animationTimingFunction: 'ease-in',
    opacity: '0'
  },
  '40%': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, -20deg)',
    animationTimingFunction: 'ease-in'
  },
  '60%': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, 10deg)',
    opacity: '1'
  },
  '80%': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, -5deg)'
  },
  'to': {
    transform: 'perspective(400px)'
  }
};

var keyframeFlipOutX = {
  'from': {
    transform: 'perspective(400px)'
  },
  '30%': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, -20deg)',
    opacity: '1'
  },
  'to': {
    transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
    opacity: '0'
  }
};

var keyframeFlipOutY = {
  'from': {
    transform: 'perspective(400px)'
  },
  '30%': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, -15deg)',
    opacity: '1'
  },
  'to': {
    transform: 'perspective(400px) rotate3d(0, 1, 0, 90deg)',
    opacity: '0'
  }
};

var keyframeFlip_1 = keyframeFlip;
var keyframeFlipInX_1 = keyframeFlipInX;
var keyframeFlipInY_1 = keyframeFlipInY;
var keyframeFlipOutX_1 = keyframeFlipOutX;
var keyframeFlipOutY_1 = keyframeFlipOutY;

var keyframeRotateIn = {
  'from': {
    transformOrigin: 'center',
    transform: 'rotate3d(0, 0, 1, -200deg)',
    opacity: '0'
  },
  'to': {
    transformOrigin: 'center',
    transform: 'translate3d(0, 0, 0)',
    opacity: '1'
  }
};

var keyframeRotateInDownLeft = {
  'from': {
    transformOrigin: 'left bottom',
    transform: 'rotate3d(0, 0, 1, -45deg)',
    opacity: '0'
  },
  'to': {
    transformOrigin: 'left bottom',
    transform: 'translate3d(0, 0, 0)',
    opacity: '1'
  }
};

var keyframeRotateInDownRight = {
  'from': {
    transformOrigin: 'right bottom',
    transform: 'rotate3d(0, 0, 1, 45deg)',
    opacity: '0'
  },
  'to': {
    transformOrigin: 'right bottom',
    transform: 'translate3d(0, 0, 0)',
    opacity: '1'
  }
};

var keyframeRotateInUpLeft = {
  'from': {
    transformOrigin: 'left top',
    transform: 'rotate3d(0, 0, 1, 45deg)',
    opacity: '0'
  },
  'to': {
    transformOrigin: 'left top',
    transform: 'translate3d(0, 0, 0)',
    opacity: '1'
  }
};

var keyframeRotateInUpRight = {
  'from': {
    transformOrigin: 'right bottom',
    transform: 'rotate3d(0, 0, 1, -90deg)',
    opacity: '0'
  },
  'to': {
    transformOrigin: 'right bottom',
    transform: 'translate3d(0, 0, 0)',
    opacity: '1'
  }
};

var keyframeRotateIn_1 = keyframeRotateIn;
var keyframeRotateInDownLeft_1 = keyframeRotateInDownLeft;
var keyframeRotateInDownRight_1 = keyframeRotateInDownRight;
var keyframeRotateInUpLeft_1 = keyframeRotateInUpLeft;
var keyframeRotateInUpRight_1 = keyframeRotateInUpRight;

var keyframeRotateOut = {
  'from': {
    transformOrigin: 'center',
    opacity: '1'
  },
  'to': {
    transformOrigin: 'center',
    transform: 'rotate3d(0, 0, 1, 200deg)',
    opacity: '0'
  }
};

var keyframeRotateOutDownLeft = {
  'from': {
    transformOrigin: 'left bottom',
    opacity: '1'
  },
  'to': {
    transformOrigin: 'left bottom',
    transform: 'rotate3d(0, 0, 1, 45deg)',
    opacity: '0'
  }
};

var keyframeRotateOutDownRight = {
  'from': {
    transformOrigin: 'right bottom',
    opacity: '1'
  },
  'to': {
    transformOrigin: 'right bottom',
    transform: 'rotate3d(0, 0, 1, -45deg)',
    opacity: '0'
  }
};

var keyframeRotateOutUpLeft = {
  'from': {
    transformOrigin: 'left bottom',
    opacity: '1'
  },
  'to': {
    transformOrigin: 'left bottom',
    transform: 'rotate3d(0, 0, 1, -45deg)',
    opacity: '0'
  }
};

var keyframeRotateOutUpRight = {
  'from': {
    transformOrigin: 'right bottom',
    opacity: '1'
  },
  'to': {
    transformOrigin: 'left bottom',
    transform: 'rotate3d(0, 0, 1, 90deg)',
    opacity: '0'
  }
};

var keyframeRotateOut_1 = keyframeRotateOut;
var keyframeRotateOutDownLeft_1 = keyframeRotateOutDownLeft;
var keyframeRotateOutDownRight_1 = keyframeRotateOutDownRight;
var keyframeRotateOutUpLeft_1 = keyframeRotateOutUpLeft;
var keyframeRotateOutUpRight_1 = keyframeRotateOutUpRight;

var keyframeRollIn = {
  'from': {
    opacity: '0',
    transform: 'translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeRollOut = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg)'
  }
};

var keyframeRollIn_1 = keyframeRollIn;
var keyframeRollOut_1 = keyframeRollOut;

var keyframeZoomIn = {
  'from': {
    opacity: '0',
    transform: 'scale3d(0.3, 0.3, 0.3)'
  },
  '50%': {
    opacity: '1'
  }
};

var keyframeZoomInDown = {
  'from': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  '60%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0)',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomInLeft = {
  'from': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  '60%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0)',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomInRight = {
  'from': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  '60%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0)',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomInUp = {
  'from': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  '60%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0)',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomIn_1 = keyframeZoomIn;
var keyframeZoomInDown_1 = keyframeZoomInDown;
var keyframeZoomInLeft_1 = keyframeZoomInLeft;
var keyframeZoomInRight_1 = keyframeZoomInRight;
var keyframeZoomInUp_1 = keyframeZoomInUp;

var keyframeZoomOut = {
  'from': {
    opacity: '1'
  },
  '50%': {
    opacity: '0',
    transform: 'scale3d(0.3, 0.3, 0.3)'
  },
  'to': {
    opacity: '0'
  }
};

var keyframeZoomOutDown = {
  '40%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  'to': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0)',
    transformOrigin: 'center bottom',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomOutLeft = {
  '40%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'scale(0.1) translate3d(-2000px, 0, 0)',
    transformOrigin: 'left center'
  }
};

var keyframeZoomOutRight = {
  '40%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'scale(0.1) translate3d(2000px, 0, 0)',
    transformOrigin: 'right center'
  }
};

var keyframeZoomOutUp = {
  '40%': {
    opacity: '1',
    transform: 'scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0)',
    animationTimingFunction: 'cubic-bezier(0.55, 0.055, 0.675, 0.19)'
  },
  'to': {
    opacity: '0',
    transform: 'scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0)',
    transformOrigin: 'center bottom',
    animationTimingFunction: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
  }
};

var keyframeZoomOut_1 = keyframeZoomOut;
var keyframeZoomOutDown_1 = keyframeZoomOutDown;
var keyframeZoomOutLeft_1 = keyframeZoomOutLeft;
var keyframeZoomOutRight_1 = keyframeZoomOutRight;
var keyframeZoomOutUp_1 = keyframeZoomOutUp;

var keyframeBounceIn = {
  'from, 20%, 40%, 60%, 80%, to': {
    animationTimingFunction: 'ease-in-out'
  },
  '0%': {
    opacity: '0',
    transform: 'scale3d(0.3, 0.3, 0.3)'
  },
  '20%': {
    transform: 'scale3d(1.1, 1.1, 1.1)'
  },
  '40%': {
    transform: 'scale3d(0.9, 0.9, 0.9)'
  },
  '60%': {
    transform: 'scale3d(1.03, 1.03, 1.03)',
    opacity: '1'
  },
  '80%': {
    transform: 'scale3d(0.97, 0.97, 0.97)'
  },
  'to': {
    opacity: '1',
    transform: 'scale3d(1, 1, 1)'
  }
};

var keyframeBounceInDown = {
  'from, 60%, 75%, 90%, to': {
    animationTimingFunction: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
  },
  '0%': {
    opacity: '0',
    transform: 'translate3d(0, -3000px, 0)'
  },
  '60%': {
    opacity: '1',
    transform: 'translate3d(0, 25px, 0)'
  },
  '75%': {
    transform: 'translate3d(0, -10px, 0)'
  },
  '90%': {
    transform: 'translate3d(0, 5px, 0)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeBounceInLeft = {
  'from, 60%, 75%, 90%, to': {
    animationTimingFunction: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
  },
  '0%': {
    opacity: '0',
    transform: 'translate3d(-3000px, 0, 0)'
  },
  '60%': {
    opacity: '1',
    transform: 'translate3d(25px, 0, 0)'
  },
  '75%': {
    transform: 'translate3d(-10px, 0, 0)'
  },
  '90%': {
    transform: 'translate3d(5px, 0, 0)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeBounceInRight = {
  'from, 60%, 75%, 90%, to': {
    animationTimingFunction: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
  },
  '0%': {
    opacity: '0',
    transform: 'translate3d(3000px, 0, 0)'
  },
  '60%': {
    opacity: '1',
    transform: 'translate3d(-25px, 0, 0)'
  },
  '75%': {
    transform: 'translate3d(10px, 0, 0)'
  },
  '90%': {
    transform: 'translate3d(-5px, 0, 0)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeBounceInUp = {
  'from, 60%, 75%, 90%, to': {
    animationTimingFunction: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
  },
  '0%': {
    opacity: '0',
    transform: 'translate3d(0, 3000px, 0)'
  },
  '60%': {
    opacity: '1',
    transform: 'translate3d(0, -20px, 0)'
  },
  '75%': {
    transform: 'translate3d(0, 10px, 0)'
  },
  '90%': {
    transform: 'translate3d(0, -5px, 0)'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeBounceIn_1 = keyframeBounceIn;
var keyframeBounceInDown_1 = keyframeBounceInDown;
var keyframeBounceInLeft_1 = keyframeBounceInLeft;
var keyframeBounceInRight_1 = keyframeBounceInRight;
var keyframeBounceInUp_1 = keyframeBounceInUp;

var keyframeBounceOut = {
  '20%': {
    transform: 'scale3d(0.9, 0.9, 0.9)'
  },
  '50%, 55%': {
    opacity: '1',
    transform: 'scale3d(1.1, 1.1, 1.1)'
  },
  'to': {
    opacity: '0',
    transform: 'scale3d(0.3, 0.3, 0.3)'
  }
};

var keyframeBounceOutDown = {
  '20%': {
    transform: 'translate3d(0, 10px, 0)'
  },
  '40%, 45%': {
    opacity: '1',
    transform: 'translate3d(0, -20px, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, 2000px, 0)'
  }
};

var keyframeBounceOutLeft = {
  '20%': {
    opacity: '1',
    transform: 'translate3d(20px, 0, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(-2000px, 0, 0)'
  }
};

var keyframeBounceOutRight = {
  '20%': {
    opacity: '1',
    transform: 'translate3d(-20px, 0, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(2000px, 0, 0)'
  }
};

var keyframeBounceOutUp = {
  '20%': {
    transform: 'translate3d(0, -10px, 0)'
  },
  '40%, 45%': {
    opacity: '1',
    transform: 'translate3d(0, 20px, 0)'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, -2000px, 0)'
  }
};

var keyframeBounceOut_1 = keyframeBounceOut;
var keyframeBounceOutDown_1 = keyframeBounceOutDown;
var keyframeBounceOutLeft_1 = keyframeBounceOutLeft;
var keyframeBounceOutRight_1 = keyframeBounceOutRight;
var keyframeBounceOutUp_1 = keyframeBounceOutUp;

var keyframeSlideInDown = {
  'from': {
    transform: 'translate3d(0, -100%, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInLeft = {
  'from': {
    transform: 'translate3d(-100%, 0, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInRight = {
  'from': {
    transform: 'translate3d(100%, 0, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};
var keyframeSlideInUp = {
  'from': {
    transform: 'translate3d(0, 100%, 0)',
    visibility: 'visible'
  },
  'to': {
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeSlideInDown_1 = keyframeSlideInDown;
var keyframeSlideInLeft_1 = keyframeSlideInLeft;
var keyframeSlideInRight_1 = keyframeSlideInRight;
var keyframeSlideInUp_1 = keyframeSlideInUp;

var keyframeSlideOutDown = {
  'from': {
    transform: 'translate3d(0, 0, 0)'
  },
  'to': {
    visibility: 'hidden',
    transform: 'translate3d(0, 100%, 0)'
  }
};
var keyframeSlideOutLeft = {
  'from': {
    transform: 'translate3d(0, 0, 0)'
  },
  'to': {
    visibility: 'hidden',
    transform: 'translate3d(-100%, 0, 0)'
  }
};
var keyframeSlideOutRight = {
  'from': {
    transform: 'translate3d(0, 0, 0)'
  },
  'to': {
    visibility: 'hidden',
    transform: 'translate3d(100%, 0, 0)'
  }
};
var keyframeSlideOutUp = {
  'from': {
    transform: 'translate3d(0, 0, 0)'
  },
  'to': {
    visibility: 'hidden',
    transform: 'translate3d(0, -100%, 0)'
  }
};

var keyframeSlideOutDown_1 = keyframeSlideOutDown;
var keyframeSlideOutLeft_1 = keyframeSlideOutLeft;
var keyframeSlideOutRight_1 = keyframeSlideOutRight;
var keyframeSlideOutUp_1 = keyframeSlideOutUp;

var keyframeFadeIn = {
  'from': {
    opacity: '0'
  },
  'to': {
    opacity: '1'
  }
};

var keyframeFadeInDown = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInDownBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -2000px, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInLeft = {
  'from': {
    opacity: '0',
    transform: 'translate3d(-100%, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInLeftBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(-2000px, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInRight = {
  'from': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInRightBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(2000px, 0, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInUp = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeInUpBig = {
  'from': {
    opacity: '0',
    transform: 'translate3d(0, 2000px, 0)'
  },
  'to': {
    opacity: '1',
    transform: 'translate3d(0, 0, 0)'
  }
};

var keyframeFadeIn_1 = keyframeFadeIn;
var keyframeFadeInDown_1 = keyframeFadeInDown;
var keyframeFadeInDownBig_1 = keyframeFadeInDownBig;
var keyframeFadeInLeft_1 = keyframeFadeInLeft;
var keyframeFadeInLeftBig_1 = keyframeFadeInLeftBig;
var keyframeFadeInRight_1 = keyframeFadeInRight;
var keyframeFadeInRightBig_1 = keyframeFadeInRightBig;
var keyframeFadeInUp_1 = keyframeFadeInUp;
var keyframeFadeInUpBig_1 = keyframeFadeInUpBig;

var keyframeFadeOut = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0'
  }
};

var keyframeFadeOutDown = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, 100%, 0)'
  }
};

var keyframeFadeOutDownBig = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, 2000px, 0)'
  }
};

var keyframeFadeOutLeft = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(-100%, 0, 0)'
  }
};

var keyframeFadeOutLeftBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(-2000px, 0, 0)'
  }
};

var keyframeFadeOutRight = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(100%, 0, 0)'
  }
};

var keyframeFadeOutRightBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(2000px, 0, 0)'
  }
};

var keyframeFadeOutUp = {
  'from': {
    opacity: '1'
  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, -100%, 0)'
  }
};

var keyframeFadeOutUpBig = {
  'from': {
    opacity: '1'

  },
  'to': {
    opacity: '0',
    transform: 'translate3d(0, -2000px, 0)'
  }
};

var keyframeFadeOut_1 = keyframeFadeOut;
var keyframeFadeOutDown_1 = keyframeFadeOutDown;
var keyframeFadeOutDownBig_1 = keyframeFadeOutDownBig;
var keyframeFadeOutLeft_1 = keyframeFadeOutLeft;
var keyframeFadeOutLeftBig_1 = keyframeFadeOutLeftBig;
var keyframeFadeOutRight_1 = keyframeFadeOutRight;
var keyframeFadeOutRightBig_1 = keyframeFadeOutRightBig;
var keyframeFadeOutUp_1 = keyframeFadeOutUp;
var keyframeFadeOutUpBig_1 = keyframeFadeOutUpBig;

var keyframeRotate = {
  'from': {
    transform: 'rotate(0deg)'
  },
  'to': {
    transform: 'rotate(360deg)'
  }
};

var keyframeRotate_1 = keyframeRotate;

var clearFix = function clearFix() {
  return {
    '&:before,&:after': {
      content: '""', // 1
      display: 'table' // 2
    },
    '&:after': {
      clear: 'both'
    }
  };
};

/* eslint-disable import/no-extraneous-dependencies */



var grid = function (_ref) {
  var $gridGutterWidth = _ref.$gridGutterWidth;

  // Grid system
  //
  // Generate semantic grid columns with these mixins.

  // Centered container element
  function containerFixed() {
    var $gutter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $gridGutterWidth;

    var result = {
      marginRight: 'auto',
      marginLeft: 'auto'
    };

    if ($gutter !== 0) {
      assign(result, {
        'padding-left': Math.floor($gutter / 2),
        'padding-right': Math.ceil($gutter / 2)
      });
    }

    return assign(result, clearFix());
  }

  // Creates a wrapper for a series of columns
  function makeRow() {
    var $gutter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $gridGutterWidth;

    return {
      'margin-left': Math.ceil($gutter / -2),
      'margin-right': Math.floor($gutter / -2)
    };
  }

  // Generate the extra small columns
  // Generate the small columns
  // Generate the medium columns
  // Generate the large columns
  return {
    containerFixed: containerFixed,
    makeRow: makeRow
  };
};

/* eslint-disable no-param-reassign,no-bitwise */

function lighten(col, amt) {
  var usePound = false;

  if (col[0] == '#') {
    col = col.slice(1);
    usePound = true;
  }

  var num = parseInt(col, 16);

  var r = (num >> 16) + amt;

  if (r > 255) r = 255;else if (r < 0) r = 0;

  var b = (num >> 8 & 0x00FF) + amt;

  if (b > 255) b = 255;else if (b < 0) b = 0;

  var g = (num & 0x0000FF) + amt;

  if (g > 255) g = 255;else if (g < 0) g = 0;

  return (usePound ? '#' : '') + String('000000' + (g | b << 8 | r << 16).toString(16)).slice(-6);
}

function darken(col, amt) {
  return lighten(col, -amt);
}

var lightenDarkenColor = {
  lighten: lighten,
  darken: darken
};
var lightenDarkenColor_1 = lightenDarkenColor.lighten;
var lightenDarkenColor_2 = lightenDarkenColor.darken;

var lighten$1 = lightenDarkenColor.lighten;

var $lightGrey = lighten$1('#c8c7cc', 18);

// WebKit-style focus

// WebKit-specific. Other browsers will keep their default outline style.
// (Initially tried to also force default via `outline: initial`,
// but that seems to erroneously remove the outline in Firefox altogether.)

var tabFocus = {
  outline: '5px auto -webkit-focus-ring-color',
  'outline-offset': '-2px'
};
var tabFocus_1 = tabFocus.outline;

var defaultOptions = {
  block: {
    '[v-cloak] .v-cloak-block': {
      display: 'block'
    },
    '.v-cloak-block': {
      display: 'none'
    }
  },

  flex: {
    '[v-cloak] .v-cloak-flex': {
      display: 'flex'
    },
    '.v-cloak-flex': {
      display: 'none'
    }
  },

  hidden: {
    '[v-cloak] .v-cloak-hidden, [v-cloak].v-cloak-hidden': {
      display: 'none'
    },
    '[v-cloak] [v-cloak-hidden]': {
      display: 'none'
    }
  },

  inline: {
    '[v-cloak] .v-cloak-inline': {
      display: 'inline'
    },
    '.v-cloak-inline': {
      display: 'none'
    }
  },

  'inline-block': {
    '[v-cloak] .v-cloak-inline-block': {
      display: 'inline-block'
    },
    '.v-cloak-inline-block': {
      display: 'none'
    }
  },

  'inline-flex': {
    '[v-cloak] .v-cloak-inline-flex': {
      display: 'inline-flex'
    },
    '.v-cloak-inline-flex': {
      display: 'none'
    }
  },

  invisible: {
    '[v-cloak] .v-cloak-invisible': {
      visibility: 'hidden'
    }
  }
};
var defaultOptions_1 = defaultOptions.block;
var defaultOptions_2 = defaultOptions.flex;
var defaultOptions_3 = defaultOptions.hidden;
var defaultOptions_4 = defaultOptions.inline;
var defaultOptions_5 = defaultOptions.invisible;

export { grid as __moduleExports, defaultOptions_1 as block, lightenDarkenColor_2 as darken, defaultOptions_2 as flex, defaultOptions_3 as hidden, defaultOptions_4 as inline, defaultOptions_5 as invisible, keyframeBounce_1 as keyframeBounce, keyframeBounceIn_1 as keyframeBounceIn, keyframeBounceInDown_1 as keyframeBounceInDown, keyframeBounceInLeft_1 as keyframeBounceInLeft, keyframeBounceInRight_1 as keyframeBounceInRight, keyframeBounceInUp_1 as keyframeBounceInUp, keyframeBounceOut_1 as keyframeBounceOut, keyframeBounceOutDown_1 as keyframeBounceOutDown, keyframeBounceOutLeft_1 as keyframeBounceOutLeft, keyframeBounceOutRight_1 as keyframeBounceOutRight, keyframeBounceOutUp_1 as keyframeBounceOutUp, keyframeFadeIn_1 as keyframeFadeIn, keyframeFadeInDown_1 as keyframeFadeInDown, keyframeFadeInDownBig_1 as keyframeFadeInDownBig, keyframeFadeInLeft_1 as keyframeFadeInLeft, keyframeFadeInLeftBig_1 as keyframeFadeInLeftBig, keyframeFadeInRight_1 as keyframeFadeInRight, keyframeFadeInRightBig_1 as keyframeFadeInRightBig, keyframeFadeInUp_1 as keyframeFadeInUp, keyframeFadeInUpBig_1 as keyframeFadeInUpBig, keyframeFadeOut_1 as keyframeFadeOut, keyframeFadeOutDown_1 as keyframeFadeOutDown, keyframeFadeOutDownBig_1 as keyframeFadeOutDownBig, keyframeFadeOutLeft_1 as keyframeFadeOutLeft, keyframeFadeOutLeftBig_1 as keyframeFadeOutLeftBig, keyframeFadeOutRight_1 as keyframeFadeOutRight, keyframeFadeOutRightBig_1 as keyframeFadeOutRightBig, keyframeFadeOutUp_1 as keyframeFadeOutUp, keyframeFadeOutUpBig_1 as keyframeFadeOutUpBig, keyframeFlash_1 as keyframeFlash, keyframeFlip_1 as keyframeFlip, keyframeFlipInX_1 as keyframeFlipInX, keyframeFlipInY_1 as keyframeFlipInY, keyframeFlipOutX_1 as keyframeFlipOutX, keyframeFlipOutY_1 as keyframeFlipOutY, keyframeHeadShake_1 as keyframeHeadShake, keyframeHeartBeat_1 as keyframeHeartBeat, keyframeHinge_1 as keyframeHinge, keyframeJackInTheBox_1 as keyframeJackInTheBox, keyframeJello_1 as keyframeJello, keyframeLightSpeedIn_1 as keyframeLightSpeedIn, keyframeLightSpeedOut_1 as keyframeLightSpeedOut, keyframePulse_1 as keyframePulse, keyframeRollIn_1 as keyframeRollIn, keyframeRollOut_1 as keyframeRollOut, keyframeRotate_1 as keyframeRotate, keyframeRotateIn_1 as keyframeRotateIn, keyframeRotateInDownLeft_1 as keyframeRotateInDownLeft, keyframeRotateInDownRight_1 as keyframeRotateInDownRight, keyframeRotateInUpLeft_1 as keyframeRotateInUpLeft, keyframeRotateInUpRight_1 as keyframeRotateInUpRight, keyframeRotateOut_1 as keyframeRotateOut, keyframeRotateOutDownLeft_1 as keyframeRotateOutDownLeft, keyframeRotateOutDownRight_1 as keyframeRotateOutDownRight, keyframeRotateOutUpLeft_1 as keyframeRotateOutUpLeft, keyframeRotateOutUpRight_1 as keyframeRotateOutUpRight, keyframeRubberBand_1 as keyframeRubberBand, keyframeShake_1 as keyframeShake, keyframeSlideInDown_1 as keyframeSlideInDown, keyframeSlideInLeft_1 as keyframeSlideInLeft, keyframeSlideInRight_1 as keyframeSlideInRight, keyframeSlideInUp_1 as keyframeSlideInUp, keyframeSlideOutDown_1 as keyframeSlideOutDown, keyframeSlideOutLeft_1 as keyframeSlideOutLeft, keyframeSlideOutRight_1 as keyframeSlideOutRight, keyframeSlideOutUp_1 as keyframeSlideOutUp, keyframeSwing_1 as keyframeSwing, keyframeTada_1 as keyframeTada, keyframeWobble_1 as keyframeWobble, keyframeZoomIn_1 as keyframeZoomIn, keyframeZoomInDown_1 as keyframeZoomInDown, keyframeZoomInLeft_1 as keyframeZoomInLeft, keyframeZoomInRight_1 as keyframeZoomInRight, keyframeZoomInUp_1 as keyframeZoomInUp, keyframeZoomOut_1 as keyframeZoomOut, keyframeZoomOutDown_1 as keyframeZoomOutDown, keyframeZoomOutLeft_1 as keyframeZoomOutLeft, keyframeZoomOutRight_1 as keyframeZoomOutRight, keyframeZoomOutUp_1 as keyframeZoomOutUp, lightenDarkenColor_1 as lighten, tabFocus_1 as outline };
