function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable global-require,import/no-extraneous-dependencies,no-tabs */

var assign = require('lodash/assign');

module.exports = function () {
  var $options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var helpers = require('./helpers/gridHelpers')($options);

  var _require = require('./helpers/grid')(helpers),
      containerFixed = _require.containerFixed,
      makeRow = _require.makeRow;

  var _require2 = require('./helpers/gridFramework')(helpers),
      makeGridColumns = _require2.makeGridColumns,
      makeGrid = _require2.makeGrid;

  var pixel = helpers.pixel,
      $screenSmMin = helpers.$screenSmMin,
      $screenMdMin = helpers.$screenMdMin,
      $screenLgMin = helpers.$screenLgMin,
      $screenXlMin = helpers.$screenXlMin,
      $containerSm = helpers.$containerSm,
      $containerMd = helpers.$containerMd,
      $containerLg = helpers.$containerLg,
      $containerXl = helpers.$containerXl;


  return function (_ref) {
    var _assign;

    var addComponents = _ref.addComponents,
        addUtilities = _ref.addUtilities;

    /**
    * Container widths
    *
    * Set the container width, and override it for fixed navbars in media queries.
    */
    var component = {
      '.container': assign({}, containerFixed(), (_assign = {}, _defineProperty(_assign, '@media (min-width: ' + pixel($screenSmMin) + ')', {
        width: pixel($containerSm)
      }), _defineProperty(_assign, '@media (min-width: ' + pixel($screenMdMin) + ')', {
        width: pixel($containerMd)
      }), _defineProperty(_assign, '@media (min-width: ' + pixel($screenLgMin) + ')', {
        width: pixel($containerLg)
      }), _defineProperty(_assign, '@media (min-width: ' + pixel($screenXlMin) + ')', {
        width: pixel($containerXl)
      }), _assign))

      /**
      * Fluid container
      *
      * Utilizes the mixin meant for fixed width containers, but without any defined
      * width for fluid, full width layouts.
      */
    };assign(component, {
      '.container-fluid': containerFixed(),
      '.container-full': containerFixed(0)
    });

    /**
    * Row
    *
    * Rows contain and clear the floats of your columns.
    *
    */
    assign(component, {
      '.row': makeRow()
    });

    /**
    * Columns
    *
    * Common styles for small and large grid columns
    *
    */
    assign(component, makeGridColumns());

    /**
    * Extra small grid
    *
    * Columns, offsets, pushes, and pulls for extra small devices like
    * smartphones.
    */
    assign(component, makeGrid());

    /**
    * small grid
    *
    * Columns, offsets, pushes, and pulls for the small device range, from phones
    * to tablets.
    */
    assign(component, _defineProperty({}, '@media (min-width: ' + pixel($screenSmMin) + ')', makeGrid('sm')));

    /**
    * medium grid
    *
    * Columns, offsets, pushes, and pulls for the small device range, from phones
    * to tablets.
    */
    assign(component, _defineProperty({}, '@media (min-width: ' + pixel($screenMdMin) + ')', makeGrid('md')));

    /**
    * large grid
    *
    * Columns, offsets, pushes, and pulls for the desktop device range.
    */
    assign(component, _defineProperty({}, '@media (min-width: ' + pixel($screenLgMin) + ')', makeGrid('lg')));

    /**
    * extra Large grid
    *
    * Columns, offsets, pushes, and pulls for the large desktop device range.
    */
    assign(component, _defineProperty({}, '@media (min-width: ' + pixel($screenXlMin) + ')', makeGrid('xl')));

    addComponents(component, { respectPrefix: false });
  };
};
