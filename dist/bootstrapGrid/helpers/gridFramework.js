var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable no-param-reassign */
var assign = require('lodash/assign');

module.exports = function (_ref) {
  var percentage = _ref.percentage,
      $gridColumns = _ref.$gridColumns,
      $gridGutterWidth = _ref.$gridGutterWidth,
      $position = _ref.$position,
      reverse = _ref.reverse;

  var _$position = _slicedToArray($position, 2),
      left = _$position[0],
      right = _$position[1];

  var globalReverse = reverse.global,
      localReverse = reverse.local;


  function makeGridColumns() {
    var list = '';

    for (var i = 1; i <= $gridColumns; i++) {
      list = (list ? list + ',' : list) + '.col-' + i + ',.col-sm-' + i + ',.col-md-' + i + ',.col-lg-' + i + ',.col-xl-' + i;
    }

    return _defineProperty({}, list, {
      position: 'relative',
      minHeight: '1px',
      paddingLeft: Math.ceil($gridGutterWidth / 2),
      paddingRight: Math.floor($gridGutterWidth / 2)
    });
  }

  function floatGridColumns($class) {
    var _list;

    var list = '';

    for (var i = 1; i <= $gridColumns; i++) {
      list = (list ? list + ',' : list) + '.col' + $class + '-' + i;
    }

    return _defineProperty({}, list, (_list = {
      float: left
    }, _defineProperty(_list, localReverse || 'placeholder', localReverse && {
      float: right
    }), _defineProperty(_list, globalReverse || 'placeholder', globalReverse && {
      float: right
    }), _list));
  }

  function calcGridColumn($index, $class, $type) {
    if ($type === 'width' && $index > 0) {
      return _defineProperty({}, '.col' + $class + '-' + $index, {
        width: percentage($index / $gridColumns)
      });
    }
    if ($type === 'push' && $index > 0) {
      var _ref5, _ref6, _ref7;

      return _defineProperty({}, '.col' + $class + '-push-' + $index, (_ref7 = {}, _defineProperty(_ref7, left, percentage($index / $gridColumns)), _defineProperty(_ref7, localReverse || 'placeholder', localReverse && (_ref5 = {}, _defineProperty(_ref5, left, 'auto'), _defineProperty(_ref5, right, percentage($index / $gridColumns)), _ref5)), _defineProperty(_ref7, globalReverse || 'placeholder', globalReverse && (_ref6 = {}, _defineProperty(_ref6, left, 'auto'), _defineProperty(_ref6, right, percentage($index / $gridColumns)), _ref6)), _ref7));
    }
    if ($type === 'push' && $index === 0) {
      var _ref11;

      return _defineProperty({}, '.col' + $class + '-push-0', (_ref11 = {}, _defineProperty(_ref11, left, 'auto'), _defineProperty(_ref11, localReverse || 'placeholder', localReverse && _defineProperty({}, right, 'auto')), _defineProperty(_ref11, globalReverse || 'placeholder', globalReverse && _defineProperty({}, right, 'auto')), _ref11));
    }
    if ($type === 'pull' && $index > 0) {
      var _ref13, _ref14, _ref15;

      return _defineProperty({}, '.col' + $class + '-pull-' + $index, (_ref15 = {}, _defineProperty(_ref15, right, percentage($index / $gridColumns)), _defineProperty(_ref15, localReverse || 'placeholder', localReverse && (_ref13 = {}, _defineProperty(_ref13, right, 'auto'), _defineProperty(_ref13, left, percentage($index / $gridColumns)), _ref13)), _defineProperty(_ref15, globalReverse || 'placeholder', globalReverse && (_ref14 = {}, _defineProperty(_ref14, right, 'auto'), _defineProperty(_ref14, left, percentage($index / $gridColumns)), _ref14)), _ref15));
    }
    if ($type === 'pull' && $index === 0) {
      var _ref19;

      return _defineProperty({}, '.col' + $class + '-pull-0', (_ref19 = {}, _defineProperty(_ref19, right, 'auto'), _defineProperty(_ref19, localReverse || 'placeholder', localReverse && _defineProperty({}, left, 'auto')), _defineProperty(_ref19, globalReverse || 'placeholder', globalReverse && _defineProperty({}, left, 'auto')), _ref19));
    }
    if ($type === 'offset') {
      var _ref21, _ref22, _ref23;

      return _defineProperty({}, '.offset' + $class + '-' + $index, (_ref23 = {}, _defineProperty(_ref23, 'margin-' + left, percentage($index / $gridColumns)), _defineProperty(_ref23, localReverse || 'placeholder', localReverse && (_ref21 = {}, _defineProperty(_ref21, 'margin-' + left, 'auto'), _defineProperty(_ref21, 'margin-' + right, percentage($index / $gridColumns)), _ref21)), _defineProperty(_ref23, globalReverse || 'placeholder', globalReverse && (_ref22 = {}, _defineProperty(_ref22, 'margin-' + left, 'auto'), _defineProperty(_ref22, 'margin-' + right, percentage($index / $gridColumns)), _ref22)), _ref23));
    }
    if ($type === 'order' && $index === 0) {
      var _ref25;

      return _defineProperty({}, '.order' + $class + '-last', (_ref25 = {
        float: right
      }, _defineProperty(_ref25, localReverse || 'placeholder', localReverse && {
        float: left
      }), _defineProperty(_ref25, globalReverse || 'placeholder', globalReverse && {
        float: left
      }), _ref25));
    }
  }

  function loopGridColumns($columns, $class, $type) {
    var result = {};

    for (var i = 0; i <= $columns; i++) {
      result = assign(result, calcGridColumn(i, $class, $type));
    }

    return result;
  }

  function makeGrid($class) {
    $class = $class ? '-' + $class : '';
    return assign({}, floatGridColumns($class), calcGridColumn(0, $class, 'order'), loopGridColumns($gridColumns, $class, 'width'), loopGridColumns($gridColumns, $class, 'pull'), loopGridColumns($gridColumns, $class, 'push'), loopGridColumns($gridColumns, $class, 'offset'));
  }

  return {
    makeGridColumns: makeGridColumns,
    makeGrid: makeGrid
  };
};
