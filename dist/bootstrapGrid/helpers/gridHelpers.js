var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function containerWidth(width) {
  return width / 1.02 - width / 1.02 % 10;
}

function parseReverse(reverse, direction) {
  if (!reverse) return reverse;

  var reverseObj = (typeof reverse === 'undefined' ? 'undefined' : _typeof(reverse)) === 'object' ? reverse : { global: true, local: true };

  if (reverseObj && reverseObj.global) {
    if (typeof reverseObj.global !== 'string') {
      reverseObj.global = direction === 'ltr' ? '.rtl' : '.ltr';
    }
    reverseObj.global = reverseObj.global + ' &';
  }

  if (reverseObj && reverseObj.local) {
    if (typeof reverseObj.local !== 'string') {
      reverseObj.local = '.col-rev';
    }
    reverseObj.local = '&' + reverseObj.local;
  }

  return reverseObj;
}

module.exports = function (options) {
  var _options$xs = options.xs,
      $screenXsMax = _options$xs === undefined ? 576 : _options$xs,
      _options$sm = options.sm,
      $screenSmMin = _options$sm === undefined ? 576 : _options$sm,
      _options$md = options.md,
      $screenMdMin = _options$md === undefined ? 768 : _options$md,
      _options$lg = options.lg,
      $screenLgMin = _options$lg === undefined ? 992 : _options$lg,
      _options$xl = options.xl,
      $screenXlMin = _options$xl === undefined ? 1200 : _options$xl,
      _options$cols = options.cols,
      $gridColumns = _options$cols === undefined ? 12 : _options$cols,
      _options$gutters = options.gutters,
      $gridGutterWidth = _options$gutters === undefined ? 30 : _options$gutters,
      _options$$containerSm = options.$containerSm,
      $containerSm = _options$$containerSm === undefined ? containerWidth($screenSmMin) : _options$$containerSm,
      _options$$containerMd = options.$containerMd,
      $containerMd = _options$$containerMd === undefined ? containerWidth($screenMdMin) : _options$$containerMd,
      _options$$containerLg = options.$containerLg,
      $containerLg = _options$$containerLg === undefined ? containerWidth($screenLgMin) : _options$$containerLg,
      _options$$containerXl = options.$containerXl,
      $containerXl = _options$$containerXl === undefined ? containerWidth($screenXlMin) : _options$$containerXl,
      _options$direction = options.direction,
      direction = _options$direction === undefined ? 'ltr' : _options$direction,
      _options$reverse = options.reverse,
      reverse = _options$reverse === undefined ? {} : _options$reverse;


  var $position = [];

  if (direction === 'ltr') {
    $position = ['left', 'right'];
  } else {
    $position = ['right', 'left'];
  }

  function percentage(number) {
    return number * 100 + '%';
  }

  function pixel(val) {
    return val + 'px';
  }

  // const $containerMd = 720 + $gridGutterWidth;
  // const $containerLg = 940 + $gridGutterWidth;
  // const $containerXl = 1140 + $gridGutterWidth;

  return {
    percentage: percentage,
    pixel: pixel,
    $gridColumns: $gridColumns,
    $gridGutterWidth: $gridGutterWidth,
    $screenXsMax: $screenXsMax,
    $screenSmMin: $screenSmMin,
    $screenMdMin: $screenMdMin,
    $screenLgMin: $screenLgMin,
    $screenXlMin: $screenXlMin,
    $containerSm: $containerSm,
    $containerMd: $containerMd,
    $containerLg: $containerLg,
    $containerXl: $containerXl,
    $position: $position,
    reverse: parseReverse(reverse, direction) || {}
  };
};
