/* eslint-disable import/no-extraneous-dependencies */
var assign = require('lodash/assign');
var clearFix = require('./../../helpers/clearFix');

module.exports = function (_ref) {
  var $gridGutterWidth = _ref.$gridGutterWidth;

  // Grid system
  //
  // Generate semantic grid columns with these mixins.

  // Centered container element
  function containerFixed() {
    var $gutter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $gridGutterWidth;

    var result = {
      marginRight: 'auto',
      marginLeft: 'auto'
    };

    if ($gutter !== 0) {
      assign(result, {
        'padding-left': Math.floor($gutter / 2),
        'padding-right': Math.ceil($gutter / 2)
      });
    }

    return assign(result, clearFix());
  }

  // Creates a wrapper for a series of columns
  function makeRow() {
    var $gutter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $gridGutterWidth;

    return {
      'margin-left': Math.ceil($gutter / -2),
      'margin-right': Math.floor($gutter / -2)
    };
  }

  // Generate the extra small columns
  // Generate the small columns
  // Generate the medium columns
  // Generate the large columns
  return {
    containerFixed: containerFixed,
    makeRow: makeRow
  };
};
